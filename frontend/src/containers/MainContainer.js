import React, {useEffect, useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {fetchLink} from "../store/actions";
import {apiURL} from "../constants";
import Notification from "../components/UI/Notification/Notification";

const MainContainer = () => {
    const dispatch = useDispatch();
    const {linkObj, error} = useSelector(state => state);
    const [options, setOptions] = useState({
        originalUrl: "",
    });
    const [show, setShow] = useState(null);


    const onOptionsChange = e => {
        const name = e.target.name;
        const value = e.target.value;

        setOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const onFormSubmit = async e => {
        e.preventDefault();
        await dispatch(fetchLink(options));
    };

    useEffect(() => {
        if (error) {
            setShow(true);
        }
    }, [error]);

    return (
        <>
            <div className="container text-center">
                <Notification body={error ? error.message : null } show={show} toggleShow={() => setShow(false)} />
                <h1 className="my-4">Shorten your link!</h1>
                <Form onSubmit={onFormSubmit}>
                <Form.Group className="mb-3">
                    <Form.Control value={options.originalUrl} onChange={onOptionsChange} name="originalUrl" type="text" placeholder="Enter your link..." />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
                {
                    linkObj
                        ? <h3 className="my-4">
                            Your link now looks like this:
                            <a className="d-block" href={`${apiURL}/${linkObj.shortUrl}`}>{`${apiURL}/${linkObj.shortUrl}`}</a>
                        </h3>
                        : null
                }
        </div>
        </>
    );
};

export default MainContainer;