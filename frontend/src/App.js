import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import MainContainer from "./containers/MainContainer";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/" exact component={MainContainer}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        </BrowserRouter>
    </div>
  );
}

export default App;
