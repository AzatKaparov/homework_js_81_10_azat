import React from 'react';
import {Col, Row, Toast, Button} from "react-bootstrap";

const Notification = ({show, toggleShow, body}) => {
    return (
        <div>
            <Row>
                <Col xs={6}>
                    <Toast className="p-2" onClose={toggleShow} show={show} delay={3000} autohide>
                        <Toast.Body>{body}</Toast.Body>
                        <Button className="btn-sm" onClick={toggleShow}>Close</Button>
                    </Toast>
                </Col>
            </Row>
        </div>
    );
};

export default Notification;