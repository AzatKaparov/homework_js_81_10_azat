import axiosApi from "../axiosApi";
export const FETCH_LINK_REQUEST = "FETCH_LINK_REQUEST";
export const FETCH_LINK_SUCCESS = "FETCH_LINK_SUCCESS";
export const FETCH_LINK_FAILURE = "FETCH_LINK_FAILURE";


export const fetchLinkRequest = () => ({type: FETCH_LINK_REQUEST});
export const fetchLinkSuccess = linkObj => ({type: FETCH_LINK_SUCCESS, payload: linkObj});
export const fetchLinkFailure = err => ({type: FETCH_LINK_FAILURE, payload: err});


export const fetchLink = data => {
    return async dispatch => {
        dispatch(fetchLinkRequest());

        try {
            const response = await axiosApi.post(`/`, data);
            dispatch(fetchLinkSuccess(response.data));
        } catch (e) {
            dispatch(fetchLinkFailure(e));
        }
    };
};