import {FETCH_LINK_FAILURE, FETCH_LINK_REQUEST, FETCH_LINK_SUCCESS} from "./actions";

const initialState = {
    linkObj: null,
    loading: false,
    error: null,
}

const linkReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_LINK_REQUEST:
            return {...state, loading: true};
        case FETCH_LINK_SUCCESS:
            return {...state, linkObj: action.payload, loading: false};
        case FETCH_LINK_FAILURE:
            return {...state, error: action.payload};
        default:
            return state;
    }
};

export default linkReducer;