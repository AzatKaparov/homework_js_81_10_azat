const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const LinkShema = new Shema({
    originalUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        unique: true,
    },
});

const Link = mongoose.model('Link', LinkShema);
module.exports = Link;