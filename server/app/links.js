const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Link = require("../models/Link");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/:shortUrl', async (req, res) => {
    try {
        const result = await Link.findOne({shortUrl: req.params.shortUrl});
        if (!result || result.length === 0) {
            return res.status(404).send({error: "Not found"});
        }
        res.status(301).redirect(result.originalUrl);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    const linkData = req.body;

    const link = new Link(linkData);

    try {
        link.shortUrl = nanoid().substr(0, 7);
        await link.save();
        res.status(200).send(link);
    } catch (error) {
        res.status(400).send(error)
    }
});

module.exports = router;